﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Currency.Application.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Currency.Services
{
    internal static class StaticItems
    {
        public const string ApiBaseLink = "http://data.fixer.io/api/";
        public const string ApiAccessKey = "f32cc8e774a68633c5f369db3c0899cf";
        public const string DateFormat = "yyyy-MM-dd";
    }
    
    public sealed class CurrencyService : ICurrencyService
    {
        private readonly ILogger<CurrencyService> _logger;
        private readonly HttpClient _client = new HttpClient { BaseAddress = new Uri(StaticItems.ApiBaseLink) };

        public CurrencyService(ILogger<CurrencyService> logger)
        {
            _logger = logger;
        }

        private async Task<double> GetConversionRateAsync(string fromCurrency, string toCurrency, DateOnly? date)
        {
            var dateStr = (date is not null) ? ((DateOnly)date).ToString(StaticItems.DateFormat) : "latest";
            var queryString = $"{dateStr}?access_key={StaticItems.ApiAccessKey}&base={fromCurrency.ToUpper()}&symbols={toCurrency.ToUpper()}";
            _logger.LogDebug("fixer.io query: {QueryString}", queryString);

            var response = await _client.GetAsync(queryString);
            response.EnsureSuccessStatusCode();
            var jsonData = await response.Content.ReadAsStringAsync();
            var data = JObject.Parse(jsonData);
            var rateToken = data["rates"]?[toCurrency.ToUpper()];

            _ = rateToken ?? throw new Exception("No rate value found for currency " + toCurrency.ToUpper());

            return double.Parse(rateToken.ToString(), CultureInfo.InvariantCulture);
        }

        public async Task<double> ConvertAmountAsync(string fromCurrency, string toCurrency, double amount, DateOnly? date = null)
        {
            var conversionRate = await GetConversionRateAsync(fromCurrency, toCurrency, date);
            _logger.LogDebug("fixer.io conversion rate: {ConversionRate}", conversionRate);
            return conversionRate * amount;
        }
    }
}