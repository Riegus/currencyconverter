namespace Currency.CLI;

public enum ExitCode
{
    Success = 0,
    InvalidArguments = 1,
    InvalidConfiguration = 2,
    ExternalError = 3,
    UnknownError = 4
}