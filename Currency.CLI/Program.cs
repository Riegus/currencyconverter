﻿using System.Globalization;
using Currency.CLI;
using Currency.Application.Interfaces;
using Currency.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
    .Enrich.FromLogContext()
    .WriteTo.Console()
    .CreateLogger();

var host = CreateHostBuilder(args).Build();

static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureServices((services) => { services.AddSingleton<ICurrencyService, CurrencyService>(); })
        .UseSerilog();

if (args.Length is not (3 or 4))
{
    Console.WriteLine(
        "Wrong number of arguments! Expecting: <currencyFrom> <currencyTo> <amount> <optional: rateDate -> yyyy-MM-dd>");
    return (int)ExitCode.InvalidArguments;
}

try
{
    var currencyFrom = args[0];
    var currencyTo = args[1];
    var amount = double.Parse(args[2], CultureInfo.InvariantCulture);
    var date = args.Length == 4 ? DateOnly.ParseExact(args[3], "yyyy-MM-dd", null) : (DateOnly?)null;
    var currencyService = (ICurrencyService?)host.Services.GetService(typeof(ICurrencyService));

    if (currencyService == null)
        return (int)ExitCode.InvalidConfiguration;

    var convertedAmount = await currencyService.ConvertAmountAsync(currencyFrom, currencyTo, amount, date);
    Console.WriteLine(
        $"Amount {amount} in {currencyFrom.ToUpper()} converted to {currencyTo.ToUpper()} is: {convertedAmount}");
    return (int)ExitCode.Success;
}
catch (FormatException formatException)
{
    Console.WriteLine("Error parsing date parameter: " + formatException.Message);
    return (int)ExitCode.ExternalError;
}
catch (HttpRequestException httpRequestException)
{
    Console.WriteLine("Couldn't connect to underlying service for getting conversion rate: " +
                      httpRequestException.Message);
    return (int)ExitCode.ExternalError;
}
catch (Exception e)
{
    Console.WriteLine("Error occured: " + e.Message);
    return (int)ExitCode.UnknownError;
}