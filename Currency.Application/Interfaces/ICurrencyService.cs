using System;
using System.Threading.Tasks;

namespace Currency.Application.Interfaces
{
    public interface ICurrencyService
    {
        Task<double> ConvertAmountAsync(string fromCurrency, string toCurrency, double amount, DateOnly? date = null);
    }
}