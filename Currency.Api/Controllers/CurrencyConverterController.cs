﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Currency.Api.Models;
using Currency.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Currency.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class CurrencyConverterController : ControllerBase
    {
        private readonly ICurrencyService _currencyService;

        public CurrencyConverterController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("latest")]
        public async Task<ActionResult<CurrencyAmountResult>>Get([FromQuery] CurrencyConversionRequest currencyConversionRequest)
        {
            return await GetCurrencyAmountResultAsync(currencyConversionRequest);
        }

        /// <param name="date">yyyy-MM-dd</param>
        /// <param name="currencyConversionRequest"></param>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{date}")]
        public async Task<ActionResult<CurrencyAmountResult>>Get([FromRoute] DateTime date, [FromQuery] CurrencyConversionRequest currencyConversionRequest)
        {
            return await GetCurrencyAmountResultAsync(currencyConversionRequest, date);
        }

        private async Task<ActionResult<CurrencyAmountResult>>GetCurrencyAmountResultAsync(CurrencyConversionRequest currencyConversionRequest, DateTime? date = null)
        {
            try
            {
                var convertedAmount =
                    await _currencyService.ConvertAmountAsync(currencyConversionRequest.FromCurrency
                        , currencyConversionRequest.ToCurrency
                        , currencyConversionRequest.Amount
                        , date != null ? DateOnly.FromDateTime((DateTime)date) : null);
                return Ok(new CurrencyAmountResult(currencyConversionRequest.ToCurrency, convertedAmount));
            }
            catch (HttpRequestException httpRequestException)
            {
                return StatusCode(StatusCodes.Status503ServiceUnavailable, "Service unavailable due to external dependencies.");
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error occured: " + e.Message);
            }
        }

    }
}