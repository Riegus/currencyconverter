using System.ComponentModel.DataAnnotations;

namespace Currency.Api.Models
{
    public sealed record CurrencyConversionRequest
    {
        [Required, MinLength(3), MaxLength(3)]
        public string FromCurrency { get; set; }
        [Required, MinLength(3), MaxLength(3)]
        public string ToCurrency { get; set; }
        [Required]
        public double Amount { get; set; }
    }

}