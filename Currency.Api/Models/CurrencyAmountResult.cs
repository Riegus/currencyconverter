﻿using System;

namespace Currency.Api.Models
{
    public sealed record CurrencyAmountResult
    (
        string Currency,
        double Amount
    );
    
}